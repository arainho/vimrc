# README #

### What is this repository for? ###

This repository holds my vimrc settings

### How do I get set up? ###

mv ~/.vimrc ~/.vimrc.mybackup

cd ~/.vim

git clone https://bitbucket.org/arainho/vimrc.git

ln -s ~/.vim/vimrc ~/.vimrc
