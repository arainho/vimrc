set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'laurentgoudet/vim-howdoi'
Plugin 'saltstack/salt-vim'
Plugin 'stephpy/vim-yaml'
Plugin 'chase/vim-ansible-yaml'
Plugin 'vim-jdaddy'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Enable syntax coloring and automatic indentation
syntax on

" Indentation
set tabstop=8
set expandtab
set softtabstop=4
set shiftwidth=4

" wildmenu
set wildmenu
set wildmode=list:longest,full

" Disable backup file
"set nobackup
"set nowritebackup


""" SaltStack
" Force using the Django template syntax file
let g:sls_use_jinja_syntax = 0


"""
""" LOOK AND FEEL
"""

" Font
set guifont=DejaVu\ Sans\ Mono\ 12
"set guifont=Monospace\ 12

" Background
set background=dark
"set background=light

" Colors
colorscheme solarized
"colors slate2
"colorscheme slate2

" Enable 256 colors in vim
set t_Co=256

" View line numbers
set number
set numberwidth=3
